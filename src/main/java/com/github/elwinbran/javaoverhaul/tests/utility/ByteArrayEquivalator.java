/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.tests.utility;

import com.github.elwinbran.javaoverhaul.base.Equivalator;

/**
 * Axiomatic implementation that is the crux of all tests.
 * Tests whether to byte arrays are exactly equal (same length and contents).
 * 
 * @author Elwin Slokker
 */
public class ByteArrayEquivalator implements Equivalator<byte[]>
{

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean areEqual(byte[] first, byte[] second)
    {
        if(first == second)
        {
            return true;
        }
        if(first.length != second.length)
        {
            return false;
        }
        for(int i = 0; i < first.length; i++)
        {
            if(first[i] != second[i])
            {
                return false;
            }
        }
        return true;
    }
    
}
