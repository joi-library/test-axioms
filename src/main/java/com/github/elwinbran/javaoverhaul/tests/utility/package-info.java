/**
 * This package contains a classes that enable testing, because they handle the 
 * most basic asserts and comparisons.
 */
package com.github.elwinbran.javaoverhaul.tests.utility;
